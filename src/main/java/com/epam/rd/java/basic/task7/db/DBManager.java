package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
    private static final String GET_USERS = "SELECT * FROM users";
    private static final String GET_TEAMS = "SELECT * FROM teams";
    private static final String CREATE_USER = "INSERT INTO users (login) VALUES (?)";
    private static final String CREATE_TEAM = "INSERT INTO teams (name) VALUES (?)";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    private static final String FIND_USER_TEAMS = "SELECT * FROM users_teams WHERE user_id = ?";
    private static final String FIND_TEAM_BY_ID = "SELECT * FROM teams WHERE id = ?";
    private static final String INSERT_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    private static final String GET_USER = "SELECT * FROM users WHERE login = ?";
    private static final String GET_TEAM = "SELECT * FROM teams WHERE name = ?";
    private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
    private static final String DELETE_USER = "DELETE FROM users WHERE id = ?";
    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }
    public List<User> findAllUsers(){
        List<User> requeredList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(getUrl());
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(GET_USERS)) {
            while (rs.next()) {
                User user = User.createUser(rs.getString("login"));
                user.setId(rs.getInt("id"));
                requeredList.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return requeredList;
    }

    public List<Team> findAllTeams(){
        List<Team> requeredList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(getUrl());
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(GET_TEAMS)) {
            while (rs.next()) {
                String login = rs.getString("name");

                Team team = new Team(login);

                requeredList.add(team);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return requeredList;
    }

    public boolean insertUser(User user){
        try (Connection conn = DriverManager.getConnection(getUrl());
             PreparedStatement stmt = conn.prepareStatement(CREATE_USER, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, user.getLogin());
            stmt.executeUpdate();

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean insertTeam(Team team){
        try (Connection connection = DriverManager.getConnection(getUrl());
             PreparedStatement statement = connection.prepareStatement(CREATE_TEAM, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, team.getName());
            statement.executeUpdate();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    team.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return true;
    }

    public boolean deleteUsers(User... users){
        for (User user : users) {
            if (user == null) return false;
            try (Connection con = DriverManager.getConnection(getUrl());
                 PreparedStatement stmt = con.prepareStatement(DELETE_USER)) {

                stmt.setInt(1, user.getId());
                stmt.executeUpdate();
                return true;
            } catch (SQLException throwables) {
                throwables.printStackTrace();

            }
        }
        return false;
    }

    public boolean deleteTeam(Team team){
        try (Connection con = DriverManager.getConnection(getUrl());
             PreparedStatement stmt = con.prepareStatement(DELETE_TEAM)) {
            stmt.setInt(1, team.getId());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public User getUser(String login){
        User user = new User();
        try (Connection con = DriverManager.getConnection(getUrl());
             PreparedStatement stmt = con.prepareStatement(GET_USER)) {
            stmt.setString(1, login);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String userLogin = rs.getString("login");

                user = new User(id, userLogin);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public Team getTeam(String name){
        Team team = new Team();
        try (Connection con = DriverManager.getConnection(getUrl());
             PreparedStatement stmt = con.prepareStatement(GET_TEAM)) {
            stmt.setString(1, name);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String teamName = rs.getString("name");

                team = new Team(id, teamName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return team;
    }

    public boolean setTeamsForUser(User user, Team... teams)throws DBException{
        if (user == null) throw new DBException("", new NullPointerException());
        try (Connection connection = DriverManager.getConnection(getUrl())) {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement(INSERT_TEAMS_FOR_USER)) {
                for (Team team : teams) {
                    if (team == null) {
                        connection.rollback();
                        throw new DBException("", new NullPointerException());
                    }

                    statement.setString(1, String.valueOf(user.getId()));
                    statement.setString(2, String.valueOf(team.getId()));
                    statement.executeUpdate();
                }

            } catch (SQLException throwables) {
                connection.rollback();
                throw new DBException("", throwables);
            }
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DBException("", throwables);
        }

        return true;
    }

    public List<Team> getUserTeams(User user){
        List<Team> teamList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(getUrl());
             PreparedStatement statement = connection.prepareStatement(FIND_USER_TEAMS)) {

            statement.setInt(1, user.getId());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                teamList.add(convertTeam(rs));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return teamList;
    }

    public boolean updateTeam(Team team){
        try (Connection con = DriverManager.getConnection(getUrl());
             PreparedStatement statement = con.prepareStatement(UPDATE_TEAM)) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private String getUrl() {
        String url = null;
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("app.properties"));
            url = properties.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    private Team convertTeam(ResultSet rs) throws SQLException {
        try (Connection con = DriverManager.getConnection(getUrl());
             PreparedStatement stmt = con.prepareStatement(FIND_TEAM_BY_ID)) {

            stmt.setInt(1, rs.getInt("team_id"));
            ResultSet resultSet = stmt.executeQuery();
            resultSet.next();
            Team team = Team.createTeam(resultSet.getString("name"));
            team.setId(resultSet.getInt("id"));
            return team;
        }
    }
}
